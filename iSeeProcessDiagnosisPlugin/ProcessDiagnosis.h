//
//  ScreenCapture.h
//  screenCapturePlugin
//
//  Created by Myren Yang on 13/05/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "UnityPluginUtility.h"

//extern "C" {

// for test
const char* PrintHello (void);
int PrintANumber (void);
int AddTwoIntegers(int, int);
float AddTwoFloats(float, float);
void testByteArray(unsigned char* array, int size);


const char* getErrorMessage(void);

int initProcessDiagnosis(void);
void destroyProcessDiagnosis(void);
const char* getCPUUsage(void);
const char* getMemoryUsage(void);

@interface ProcessDiagnosis : NSObject

-(NSString*) getCPUUsage;
- (NSString*) getMemoryUsage;

@end
