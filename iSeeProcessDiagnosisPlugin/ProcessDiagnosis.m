//
//  ScreenCapture.m
//  screenCapturePlugin
//
//  Created by Myren Yang on 13/05/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "ProcessDiagnosis.h"

#import <sys/sysctl.h>
#import <sys/types.h>
#import <mach/mach.h>
#import <mach/processor_info.h>
#import <mach/mach_host.h>


// for plugin testing
const char* PrintHello(){
	return "Hello World";
}

int PrintANumber(){
	return 5;
}

int AddTwoIntegers(int a, int b) {
	return a + b;
}

float AddTwoFloats(float a, float b) {
	return a + b;
}
void testByteArray(unsigned char* array, int size) 
{
    for (int i = 0; i < size; i++)
    {
        array[i] = (unsigned char) (size - array[i]);
    }
}
// end plugin testing

static NSString *errorMessage = @"";
static ProcessDiagnosis* processDiagnosis = nil;
static processor_info_array_t cpuInfo, prevCpuInfo;
static mach_msg_type_number_t numCpuInfo, numPrevCpuInfo;
static unsigned numCPUs;
static NSTimer *updateTimer;
static NSLock *CPUUsageLock;
static float preProcessUse;


const char* getErrorMessage(void)
{
    NSString * temp = errorMessage;
    errorMessage = @"";
    return [temp cStringUsingEncoding:NSASCIIStringEncoding];
}

int initProcessDiagnosis() {
    processDiagnosis = [[ProcessDiagnosis alloc] init];
    if (processDiagnosis == nil) {
        errorMessage = @"Failed to create processDiagnosis instance";
        return -1;
    }
    
    return 0;
}

void destroyProcessDiagnosis() {
    if (processDiagnosis != nil)
    {
        [processDiagnosis release];
    }
}

const char* getCPUUsage(void) {
    char* res;
    if (processDiagnosis == nil) {
        errorMessage = @"Please init ProcessDiagnosisPlugin";
        return -1;
    }
    NSString * temp = [processDiagnosis getCPUUsage];
    res = cStringCopy([temp UTF8String]);
    errorMessage = @"";
    return res;
}

const char* getMemoryUsage(void) {
    char* res;
    if (processDiagnosis == nil) {
        errorMessage = @"Please init ProcessDiagnosisPlugin";
        return -1;
    }
    NSString * temp = [processDiagnosis getMemoryUsage];
    res = cStringCopy([temp UTF8String]);
    errorMessage = @"";
    return res;
}

@implementation ProcessDiagnosis

-(id)init {
    if((self = [super init])) {
        int mib[2U] = { CTL_HW, HW_NCPU };
        size_t sizeOfNumCPUs = sizeof(numCPUs);
        int status = sysctl(mib, 2U, &numCPUs, &sizeOfNumCPUs, NULL, 0U);
        if(status)
            numCPUs = 1;
        
        CPUUsageLock = [[NSLock alloc] init];
        preProcessUse = 0;
        [self getCPUUsage];
    }
    return self;
}

// return string as format "<process_percentage> <all_percentage>"
-(NSString*) getCPUUsage {
    NSString* resString = nil;
    float processUse = 0;
    float totalCpu = 0, totalInUse = 0;
    pid_t curPid = getpid();
    
    natural_t numCPUsU = 0U;
    kern_return_t err = host_processor_info(mach_host_self(), PROCESSOR_CPU_LOAD_INFO, &numCPUsU, &cpuInfo, &numCpuInfo);
    
    if(err == KERN_SUCCESS) {
        [CPUUsageLock lock];
        
        processUse = [self get_process_usage_by_pid:curPid]*1000/4;
        
        for(unsigned i = 0U; i < numCPUs; ++i) {
            float inUse, total;
            if(prevCpuInfo) {
                inUse = (
                         (cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_USER]   - prevCpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_USER])
                         + (cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_SYSTEM] - prevCpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_SYSTEM])
                         + (cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_NICE]   - prevCpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_NICE])
                         );
                total = inUse + (cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_IDLE] - prevCpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_IDLE]);
            } else {
                inUse = cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_USER] + cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_SYSTEM] + cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_NICE];
                total = inUse + cpuInfo[(CPU_STATE_MAX * i) + CPU_STATE_IDLE];
            }
            
            totalInUse += inUse;
            totalCpu += total;
            
//            NSLog(@"Core: %u, inUser: %f, total: %f, Usage: %f",i,inUse, total,inUse / total);
        }
        
        [CPUUsageLock unlock];
//        NSLog(@"get_process_usage:%f", processUse - preProcessUse);
//        NSLog(@"AllUsagePercentage:%f", totalInUse*100/totalCpu);
//        NSLog(@"ProcessUsagePercentage:%f", (processUse - preProcessUse)*100/totalCpu/4);
        
        resString = [NSString stringWithFormat:@"%d %d", (int)(totalInUse*100/totalCpu), (int)((processUse - preProcessUse)*100/totalCpu)];
        
        if(prevCpuInfo) {
            size_t prevCpuInfoSize = sizeof(integer_t) * numPrevCpuInfo;
            vm_deallocate(mach_task_self(), (vm_address_t)prevCpuInfo, prevCpuInfoSize);
        }
        prevCpuInfo = cpuInfo;
        numPrevCpuInfo = numCpuInfo;
        
        preProcessUse = processUse;
        
        cpuInfo = NULL;
        numCpuInfo = 0U;
        
        return resString;
    } else {
        errorMessage = [NSString stringWithFormat:@"%s", mach_error_string(err)];
//        [NSApp terminate:nil];
        return errorMessage;
    }

}

- (float) get_process_usage_by_pid:(int) pid
{
    float processUsage = 0;
    NSTask *task;
    task = [[NSTask alloc] init];
    [task setLaunchPath:@"/bin/ps"];
    
    NSArray *arguments;
    arguments = [NSArray arrayWithObjects: @"-O",@"%cpu",@"-p",[NSString stringWithFormat:@"%d",pid], nil];
    [task setArguments: arguments];
    
    NSPipe *pipe;
    pipe = [NSPipe pipe];
    [task setStandardOutput: pipe];
    
    NSFileHandle *file;
    file = [pipe fileHandleForReading];
    
    [task launch];
    
    NSData *data;
    data = [file readDataToEndOfFile];
    
    NSString *string;
    string = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    
    NSString* temp = [string stringByReplacingOccurrencesOfString:@"PID  %CPU   TT  STAT      TIME COMMAND" withString:@""];
    
    NSMutableArray* arr =(NSMutableArray*) [temp componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [arr removeObject:@""];
    
    NSString* ret;
    if([arr count]>=5)
        ret = [arr objectAtIndex:4];
    else
        return processUsage;
    
    NSArray* times = [ret componentsSeparatedByString:@":"];
    processUsage = [times[0] floatValue] * 60;
    processUsage += [times[1] floatValue];
    
    return processUsage;
}


- (NSString*) getMemoryUsage {
    NSString* retString;
    double processUse, totalRAM, totalUsed;
    
    
    struct task_basic_info t_info;
    mach_msg_type_number_t t_info_count = TASK_BASIC_INFO_COUNT;
    
    kern_return_t err = task_info(mach_task_self(),
                                  TASK_BASIC_INFO, (task_info_t)&t_info,
                                  &t_info_count);
    if (KERN_SUCCESS != err)
    {
        errorMessage = [NSString stringWithFormat:@"%s", mach_error_string(err)];
        return errorMessage;
    }
    // resident size is in t_info.resident_size;
//    NSLog(@"resident size:%ld", t_info.resident_size);
    processUse = t_info.resident_size;
    // virtual size is in t_info.virtual_size;
//    NSLog(@"virtual size:%ld", t_info.virtual_size);
    
    vm_size_t page_size;
    mach_port_t mach_port;
    mach_msg_type_number_t count;
    vm_statistics64_data_t vm_stats;
    
    mach_port = mach_host_self();
    count = sizeof(vm_stats) / sizeof(natural_t);
    if (KERN_SUCCESS == host_page_size(mach_port, &page_size) &&
        KERN_SUCCESS == host_statistics64(mach_port, HOST_VM_INFO,
                                          (host_info64_t)&vm_stats, &count))
    {
        long long free_memory = (int64_t)vm_stats.free_count * (int64_t)page_size;
        
        long long used_memory = ((int64_t)vm_stats.active_count +
                                 (int64_t)vm_stats.inactive_count +
                                 (int64_t)vm_stats.wire_count) *  (int64_t)page_size;
        totalUsed = used_memory;
        totalRAM = free_memory+used_memory;
    }
    else
    {
        errorMessage = @"Failed to get system memory usage.";
        return errorMessage;
    }

    retString = [NSString stringWithFormat:@"%d %d", (int)(processUse*100.0/totalRAM), (int)(totalUsed*100.0/totalRAM)];
    return retString;
}

@end


